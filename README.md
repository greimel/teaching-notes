# Launch in Pluto.jl notebooks in binder

(takes half a minute to launch)

* [two-variables.jl](https://mybinder.org/v2/gh/fonsp/pluto-on-binder/master?urlpath=pluto/open?url=https%253A%252F%252Fgitlab.com%252Fgreimel%252Fteaching-notes%252F-%252Fraw%252Fmaster%252Ftwo-variables.jl)

* [linear-approximation.jl](https://mybinder.org/v2/gh/fonsp/pluto-on-binder/master?urlpath=pluto/open?url=https%253A%252F%252Fgitlab.com%252Fgreimel%252Fteaching-notes%252F-%252Fraw%252Fmaster%252Flinear-approximation.jl)

* [demand.jl](https://mybinder.org/v2/gh/fonsp/pluto-on-binder/master?urlpath=pluto/open?url=https%253A%252F%252Fgitlab.com%252Fgreimel%252Fteaching-notes%252F-%252Fraw%252Fmaster%252Fdemand.jl)

   Note that there is an issue with using SymPy on binder. Uncomment the second cell. Building takes three minutes. 